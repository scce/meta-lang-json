package info.scce.cinco.data.json.generator;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.databind.json.JsonMapper.Builder;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.dataformat.yaml.YAMLGenerator;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;

import de.jabc.cinco.meta.plugin.generator.runtime.IGenerator;
import info.scce.cinco.data.json.metajson.Assignment;
import info.scce.cinco.data.json.metajson.Format;
import info.scce.cinco.data.json.metajson.*;


public class JSONGenerator implements IGenerator<MetaJSON> {

	private ObjectMapper mapper;

	private Builder createBuilder(Format format) {
		
		return JsonMapper.builder(
			format == Format.YAML 
				? YAMLFactory.builder().enable(YAMLGenerator.Feature.MINIMIZE_QUOTES).build()
				: JsonFactory.builder().build())
	//	  	.addModule(new ParameterNamesModule())
			.addModule(new Jdk8Module())
	//	   .addModule(new JavaTimeModule())
			.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
	//		.configure(SerializationFeature.WRITE_EMPTY_JSON_ARRAYS, false)
			.serializationInclusion(JsonInclude.Include.NON_EMPTY)
			.serializationInclusion(Include.NON_ABSENT)
			.disable(DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE)
			.configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET, false)
			.configure(JsonParser.Feature.AUTO_CLOSE_SOURCE, false);
	}
	
	@Override
	public void generate (MetaJSON data, IPath path, IProgressMonitor monitor) {
		
		IPath targetFile = path.append(data.getFileName());
		try (PrintWriter writer = new PrintWriter(targetFile.toFile())) {
			
			Builder builder = createBuilder(data.getFormat());
			if (data.isPretty()) {
				builder = builder.enable(SerializationFeature.INDENT_OUTPUT);
			}
			mapper = builder.build();
			
			mapper.writeValue(writer, data.getDataElements().stream()
				.filter(e -> e.getOutgoing().size() == 0)
				.findFirst().map(this::convert).get());
			
		} catch (IOException e) {
			e.printStackTrace();
			
			Display.getDefault().asyncExec(
				() -> MessageDialog.openError(null, 
					"Generation failed", e.getLocalizedMessage() 
					+ "\n\n (EN): \t" + e.getMessage()));
		}
	}
	
	private Object convert (DataElement element) {
		if (element instanceof Dictionary)
			return convert((Dictionary) element);
		if (element instanceof info.scce.cinco.data.json.metajson.List)
			return convert((info.scce.cinco.data.json.metajson.List) element);
		if (element instanceof Literal)
			return convert((Literal) element);
		return null;
	}
	
	private Map<String, ?> convert (Dictionary object) {
		final Collector<Assignment, ?, Map<String, Object>> toSortedMap = Collectors.toMap (
				(Assignment assignment) -> assignment.getName(), 
				(Assignment assignment) -> convert(assignment.getSourceElement()),
				(a, b) -> { throw new IllegalArgumentException("Found two assignments with the same name");}, 
				LinkedHashMap::new);
		
		return object.getIncoming().stream()
				.sorted((a, b) -> a.getOrder() - b.getOrder())
				.collect(toSortedMap);
	}
	
	private List<?> convert (info.scce.cinco.data.json.metajson.List array) {
		return array.getIncoming().stream()
				.sorted((a, b) -> a.getOrder() - b.getOrder())
				.map(assignment -> convert(assignment.getSourceElement()))
				.collect(Collectors.toList());
	}
	
	private Object convert (Literal value) {
		return value.getValue();
	}
}