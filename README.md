# ![](./docs/Logo.png) JSON Meta Language

Model-based, Visual Meta Language for visual composition and authoring of JSON
Objects. Supports YAML export and JSON pretty-printing.

The goal of this language is to be a transformation target for other languages
that wish to produce JSON or YAML output.
One concrete example is [Rig](https://gitlab.com/scce/rig), where this DSL is
slated to be used as SOS transformation target to generate valid YAML files.

## Example

The following graph is a valid MetaJSON configuration, depicting a CI/CD
Pipeline for GitLab (`.gitlab-ci.yaml`):

![YAML CI/CDPipeline](./docs/Pipeline.png)

Generating YAML for this configuration yields:

````yaml
---
variables:
  NUGET_PACKAGE_REPOSITORY: .nuget/
stages:
- build
- test
- deploy
Build:
  stage: Build
  script:
  - dotnet Build -C Release
  before_script:
  - dotnet restore --packages $NUGET_PACKAGE_REPOSITORY
  image: mcr.microsoft.com/dotnet/sdk:5.0
  artifacts:
    name: app
    paths:
    - .bin/Release/.netcoreapp3.1/publish
Test:
  stage: Test
  needs:
  - Build
  coverage: "/Total[^|]*\\|[^|]*\\s+([\\d\\.]+)/"
  image: mcr.microsoft.com/dotnet/sdk:5.0
  script:
  - dotnet test
  before_script:
  - dotnet restore --packages $NUGET_PACKAGE_REPOSITORY
  artifacts:
    name: test-results
    paths:
    - '**/test-results.xml'
    when: always
    reports:
      junit: '**/test-results.xml'
````
Which is a valid, lintable CI/CD pipeline config.
## Status

This is currently a *proof of concept*. While it works reasonably well, it has
not been rigorously tested.

## How to build

**Requirements:**

* CINCO 2.1.0-SNAPSHOT

Simply cloning the project and importing it in Cinco will yield many errors due
to missing imports & bundles.

The required bundles can be found in https://gitlab.com/scce/p2-maven.

In Eclipse/Cinco, go to <kbd>Help > Install new software</kbd>

* In the *Work with:* field, type `https://scce.gitlab.io/p2-maven/main/repository`
* Install the wanted/needed bundles
* Eclipse might warn about installing unsigned bundles,
	it is safe to ignore the warning

Afterwards, you should be able to build the Cinco Product and use it.
## Maintainer

* Sebastian Teumert :: @s.teumert

# License

*JSON Meta Language* is licensed under the [Eclipse Public License 2.0](/LICENSE.txt)
