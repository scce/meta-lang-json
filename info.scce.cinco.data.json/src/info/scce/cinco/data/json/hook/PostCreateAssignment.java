package info.scce.cinco.data.json.hook;

import de.jabc.cinco.meta.runtime.hook.CincoPostCreateHook;
import info.scce.cinco.data.json.metajson.Assignment;

public class PostCreateAssignment extends CincoPostCreateHook<Assignment> {

	@Override
	public void postCreate (Assignment assignment) {
		assignment.setOrder(
			assignment.getTargetElement()
				.getIncomingAssignments().stream()
					.mapToInt(Assignment::getOrder)
					.max()
					.orElseGet(() -> -1) + 1);
	}
}
